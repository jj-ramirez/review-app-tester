module.exports = {
  pathPrefix: '/review-app-tester', // ignored unless build called with --prefix-paths
  siteMetadata: {
    title: `Sarah GHP Art Site`,
    description: `Sarah Groff Hennigh-Palermo x Codie art site.`,
    author: `@sarahgp`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
      },
    }
  ],
}
