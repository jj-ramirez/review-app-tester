import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import { Helmet } from 'react-helmet'

import './layout.css'

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={(data) => (
      <>
       <Helmet>
         <script
           data-project-id='11790219'
           data-merge-request-id='1'
           data-mr-url='https://gitlab.com'
           data-project-path='sarahghp/review-app-tester'
           src='https://host-island.sarahghp.now.sh/review-toolbar.js'
           id='review-app-toolbar-script'
         />
      </Helmet>
        <div
          style={{
            margin: '0 auto',
            padding: '5vh',
            minHeight: '100vh',
            maxWidth: '80vw',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          {children}

        </div>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
