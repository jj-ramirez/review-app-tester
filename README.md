Test app for gitlab-org/gitlab-ee#10761.

Deployed at [https://review-app-test.sarahghp.now.sh/](https://review-app-test.sarahghp.now.sh/) & [https://sarahghp.gitlab.io/review-app-tester/](https://sarahghp.gitlab.io/review-app-tester/).
